package ru.t1.akolobov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Optional;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    private final String TABLE_NAME = "tm_project";
    @NotNull
    private final String NAME_COLUMN = "name";
    @NotNull
    private final String DESCRIPTION_COLUMN = "description";
    @NotNull
    private final String STATUS_COLUMN = "status";

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @SneakyThrows
    public void checkDatabaseTable() {
        @NotNull final String sql = String.format(
                "CREATE TABLE IF NOT EXISTS %s " +
                        "( " +
                        ID_COLUMN + " character(36) PRIMARY KEY," +
                        CREATED_COLUMN + " timestamp," +
                        USER_ID_COLUMN + " character(36) REFERENCES tm_user (id)," +
                        NAME_COLUMN + " varchar(255)," +
                        DESCRIPTION_COLUMN + " text," +
                        STATUS_COLUMN + " varchar(255)" +
                        ");",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Project fetch(@NotNull ResultSet rowSet) {
        @NotNull Project project = new Project();
        project.setId(rowSet.getString(ID_COLUMN));
        project.setCreated(rowSet.getTimestamp(CREATED_COLUMN));
        project.setName(rowSet.getString(NAME_COLUMN));
        project.setDescription(rowSet.getString(DESCRIPTION_COLUMN));
        project.setStatus(Optional
                .ofNullable(Status.toStatus(rowSet.getString(STATUS_COLUMN)))
                .orElse(Status.IN_PROGRESS)
        );
        project.setUserId(rowSet.getString(USER_ID_COLUMN));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull Project model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s) "
                        + "VALUES(?, ?, ?, ?, ?, ? )",
                getTableName(),
                ID_COLUMN,
                CREATED_COLUMN,
                NAME_COLUMN,
                DESCRIPTION_COLUMN,
                STATUS_COLUMN,
                USER_ID_COLUMN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getName());
            statement.setString(4, model.getDescription());
            statement.setString(5, model.getStatus().name());
            statement.setString(6, model.getUserId());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project update(@NotNull Project model) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? "
                        + "WHERE %s = ?",
                getTableName(),
                NAME_COLUMN,
                DESCRIPTION_COLUMN,
                STATUS_COLUMN,
                USER_ID_COLUMN,
                ID_COLUMN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getName());
            statement.setString(2, model.getDescription());
            statement.setString(3, model.getStatus().name());
            statement.setString(4, model.getUserId());
            statement.setString(5, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

}
