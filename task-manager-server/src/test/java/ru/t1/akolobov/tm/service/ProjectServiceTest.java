package ru.t1.akolobov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.IProjectService;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.field.*;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.repository.ProjectRepository;
import ru.t1.akolobov.tm.repository.UserRepository;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestProject.createProject;
import static ru.t1.akolobov.tm.data.TestProject.createProjectList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    private final static PropertyService propertyService = new PropertyService();
    private final static IConnectionService connectionService = new ConnectionService(propertyService);
    private final static Connection repositoryConnection = connectionService.getConnection();
    private final static IProjectRepository repository = new ProjectRepository(repositoryConnection);
    private final static IUserRepository userRepository = new UserRepository(repositoryConnection);
    private final IProjectService service = new ProjectService(connectionService);

    @BeforeClass
    @SneakyThrows
    public static void addUsers() {
        userRepository.add(USER1);
        userRepository.add(USER2);
        repositoryConnection.commit();
    }

    @AfterClass
    @SneakyThrows
    public static void clearUsers() {
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        repositoryConnection.commit();
        repositoryConnection.close();
    }

    @Before
    @SneakyThrows
    public void initRepository() {
        repository.add(createProjectList(USER1_ID));
        repositoryConnection.commit();
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        repository.clear(USER1_ID);
        repository.clear(USER2_ID);
        repositoryConnection.commit();
    }

    @Test
    public void add() {
        Project project = createProject(USER1_ID);
        Object result = service.add(USER1_ID, project);
        Assert.assertNotNull(result);
        Assert.assertEquals(project, result);
        Assert.assertEquals(project, repository.findOneById(project.getUserId(), project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(USER_EMPTY_ID, project));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(USER_EMPTY_ID));
        List<Project> projectList = createProjectList(USER2_ID);
        service.add(projectList);
        Assert.assertFalse(service.findAll(USER2_ID).isEmpty());
        service.clear(USER2_ID);
        Assert.assertTrue(service.findAll(USER2_ID).isEmpty());
    }

    @Test
    public void existById() {
        Project project = createProject(USER1_ID);
        service.add(project);
        Assert.assertTrue(service.existById(USER1_ID, project.getId()));
        Assert.assertFalse(service.existById(USER2_ID, project.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1_ID, USER_EMPTY_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(USER_EMPTY_ID, project.getId()));
    }

    @Test
    public void findAll() {
        List<Project> projectList = createProjectList(USER2_ID);
        service.add(projectList);
        Assert.assertEquals(projectList, service.findAll(USER2_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(USER_EMPTY_ID));
    }


    @Test
    public void findAllSorted() {
        @NotNull Project project = createProject(USER1_ID);
        project.setName("project-0");
        project.setDescription("project-0-desc");
        @NotNull List<Project> projectList = new ArrayList<>();
        projectList.add(project);
        projectList.addAll(service.findAll(USER1_ID));
        service.add(project);
        Assert.assertEquals(projectList, service.findAll(USER1_ID, Sort.BY_NAME));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findAll(USER_EMPTY_ID, Sort.BY_NAME)
        );
    }

    @Test
    public void findOneById() {
        @NotNull Project project = createProject(USER1_ID);
        service.add(project);
        Assert.assertEquals(project, service.findOneById(USER1_ID, project.getId()));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneById(USER_EMPTY_ID, project.getId())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.findOneById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    @Ignore
    public void findOneByIndex() {
        @NotNull Project project = createProject(USER1_ID);
        service.add(project);
        int indexForCheck = service.findAll(USER1_ID).indexOf(project);
        service.add(createProjectList(USER1_ID));
        Assert.assertEquals(project, service.findOneByIndex(USER1_ID, indexForCheck));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneByIndex(USER_EMPTY_ID, indexForCheck)
        );
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> service.findOneByIndex(USER1_ID, service.findAll(USER1_ID).size())
        );
    }

    @Test
    public void getSize() {
        int size = service.findAll(USER1_ID).size();
        Assert.assertEquals(size, service.getSize(USER1_ID).intValue());
        service.add(createProject(USER1_ID));
        Assert.assertEquals(size + 1, service.getSize(USER1_ID).intValue());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(USER_EMPTY_ID));
    }

    @Test
    public void remove() {
        int size = service.findAll(USER1_ID).size();
        Project project = service.findOneByIndex(USER1_ID, 0);
        Assert.assertNotNull(project);
        service.remove(USER1_ID, project);
        Assert.assertFalse(service.findAll(USER1_ID).contains(project));
        Assert.assertNull(service.findOneById(USER1_ID, project.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(USER_EMPTY_ID, project));
    }

    @Test
    public void removeById() {
        int size = service.findAll(USER1_ID).size();
        Project project = service.findOneByIndex(USER1_ID, 0);
        Assert.assertNotNull(project);
        service.removeById(USER1_ID, project.getId());
        Assert.assertFalse(service.findAll(USER1_ID).contains(project));
        Assert.assertNull(service.findOneById(USER1_ID, project.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(USER_EMPTY_ID, project.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(USER1_ID, USER_EMPTY_ID));
    }

    @Test
    @Ignore
    public void removeByIndex() {
        int size = service.findAll(USER1_ID).size();
        Project project = service.findOneByIndex(USER1_ID, 0);
        Assert.assertNotNull(project);
        service.removeByIndex(USER1_ID, 0);
        Assert.assertFalse(service.findAll(USER1_ID).contains(project));
        Assert.assertNull(service.findOneById(USER1_ID, project.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(USER_EMPTY_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(USER1_ID, size));
    }

    @Test
    public void changeStatusById() {
        Project project = service.add(USER1_ID, createProject(USER1_ID));
        Assert.assertNotNull(project);
        String newProjectId = project.getId();
        Assert.assertNotNull(service.changeStatusById(USER1_ID, newProjectId, Status.IN_PROGRESS));
        project = service.findOneById(USER1_ID, newProjectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeStatusById(USER_EMPTY_ID, newProjectId, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.changeStatusById(USER1_ID, USER_EMPTY_ID, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusById(USER1_ID, newProjectId, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.changeStatusById(USER2_ID, newProjectId, Status.IN_PROGRESS)
        );
    }

    @Test
    @Ignore
    public void changeStatusByIndex() {
        int index = 0;
        Assert.assertNotNull(service.changeStatusByIndex(USER1_ID, index, Status.IN_PROGRESS));
        Project project = service.findOneByIndex(USER1_ID, index);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeStatusByIndex(USER_EMPTY_ID, index, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> service.changeStatusByIndex(USER1_ID, service.findAll(USER1_ID).size(), Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusByIndex(USER1_ID, index, null)
        );
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> service.changeStatusByIndex(USER2_ID, index, Status.IN_PROGRESS)
        );
    }

    @Test
    public void create() {
        Project project = createProject(USER1_ID);
        Project newProject = service.create(USER1_ID, project.getName());
        Assert.assertNotNull(newProject);
        newProject = service.findOneById(USER1_ID, newProject.getId());
        Assert.assertNotNull(newProject);
        Assert.assertEquals(project.getName(), newProject.getName());

        newProject = service.create(USER1_ID, project.getName(), project.getDescription());
        Assert.assertNotNull(newProject);
        newProject = service.findOneById(USER1_ID, newProject.getId());
        Assert.assertNotNull(newProject);
        Assert.assertEquals(project.getName(), newProject.getName());
        Assert.assertEquals(project.getDescription(), newProject.getDescription());

        project.setStatus(Status.IN_PROGRESS);
        newProject = service.create(USER1_ID, project.getName(), project.getStatus());
        Assert.assertNotNull(newProject);
        newProject = service.findOneById(USER1_ID, newProject.getId());
        Assert.assertNotNull(newProject);
        Assert.assertEquals(project.getName(), newProject.getName());
        Assert.assertEquals(project.getStatus(), newProject.getStatus());

        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(USER_EMPTY_ID, project.getName()));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(USER1_ID, ""));
    }

    @Test
    public void updateById() {
        Project project = service.findOneByIndex(USER1_ID, 0);
        Assert.assertNotNull(project);
        String newName = "NewName";
        String newDescription = "NewDescription";
        service.updateById(USER1_ID, project.getId(), newName, newDescription);
        Project newProject = service.findOneById(USER1_ID, project.getId());
        Assert.assertNotNull(newProject);
        Assert.assertEquals(newName, newProject.getName());
        Assert.assertEquals(newDescription, newProject.getDescription());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateById(USER_EMPTY_ID, project.getId(), newName, newDescription)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateById(USER1_ID, USER_EMPTY_ID, newName, newDescription)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateById(USER1_ID, project.getId(), "", newDescription)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.updateById(USER2_ID, project.getId(), newName, newDescription)
        );
    }

    @Test
    @Ignore
    public void updateByIndex() {
        int index = 0;
        Assert.assertNotNull(service.findOneByIndex(USER1_ID, index));
        String newName = "NewName";
        String newDescription = "NewDescription";
        service.updateByIndex(USER1_ID, index, newName, newDescription);
        Project newProject = service.findOneByIndex(USER1_ID, index);
        Assert.assertNotNull(newProject);
        Assert.assertEquals(newName, newProject.getName());
        Assert.assertEquals(newDescription, newProject.getDescription());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateByIndex(USER_EMPTY_ID, index, newName, newDescription)
        );
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> service.updateByIndex(USER1_ID, service.findAll(USER1_ID).size(), newName, newDescription)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateByIndex(USER1_ID, index, "", newDescription)
        );
    }

}
